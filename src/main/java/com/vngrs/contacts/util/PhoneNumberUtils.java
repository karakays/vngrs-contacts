package com.vngrs.contacts.util;

import java.util.Locale;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.vngrs.contacts.domain.PhoneNumber;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PhoneNumberUtils {
    private static final String DIAL_CODE = "+";

    private static final String DEFAULT_REGION = Locale.UK.getISO3Country();

    private static final PhoneNumberUtil util = PhoneNumberUtil.getInstance();

    public static PhoneNumber parse(String value) {
        com.google.i18n.phonenumbers.Phonenumber.PhoneNumber phoneNumber = doParse(value);
        return new PhoneNumber(String.valueOf(phoneNumber.getCountryCode()), String.valueOf(phoneNumber.getNationalNumber()));
    }

    public static boolean isValid(String value){
        try{
            return util.isValidNumber(doParse(value));
        }catch (Exception exception){
            return false;
        }
    }

    private static com.google.i18n.phonenumbers.Phonenumber.PhoneNumber doParse(String value) {
        try {
            value = !value.startsWith(DIAL_CODE) ? DIAL_CODE + value : value;
            return util.parse(value.trim() ,DEFAULT_REGION);
        } catch (NumberParseException exception) {
            throw new RuntimeException("Invalid phone number " + value);
        }
    }
}
