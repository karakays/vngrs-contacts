package com.vngrs.contacts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vngrs.contacts.domain.Contact;
import com.vngrs.contacts.domain.ContactPhoneNumber;
import com.vngrs.contacts.repository.ContactRepository;
import com.vngrs.contacts.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {
    
    private final ContactRepository repository;
    
    public ContactServiceImpl(ContactRepository repository) {
        this.repository = repository;
    }

    /**
     * Creates/Updates the contact including its phone-numbers.
     * @param new or existing contact
     * @return updated contact
     */
    @Override
    @Transactional(readOnly = false)
    public Contact create(Contact contact) {
        Optional<Contact> optional = repository.findByFirstNameAndLastName(contact.getFirstName(), contact.getLastName());
        if(optional.isPresent()) {
            Contact existing = optional.get();
            Set<ContactPhoneNumber> phones = existing.getPhoneNumber();
            phones.addAll(contact.getPhoneNumber());
            contact.setId(existing.getId());
            contact.setPhoneNumber(phones);
        }
        return repository.save(contact);
    }

    @Override
    public List<Contact> loadByName(String name) {
        return repository.findByFirstName(name);
    }

    @Override
    public Optional<Contact> loadById(Long contactId) {
        return Optional.ofNullable(repository.findOne(contactId));
    }

    @Override
    @Transactional(readOnly = false)
    public List<Contact> create(List<Contact> contacts) {
        List<Contact> result = new ArrayList<>();
        contacts.forEach((c) -> result.add(create(c)));
        return result;
    }

    @Override
    public List<Contact> load() {
        return repository.findAll();
    }
}
