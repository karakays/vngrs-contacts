package com.vngrs.contacts.service;

import java.util.List;
import java.util.Optional;

import com.vngrs.contacts.domain.Contact;

public interface ContactService {
    Contact create(Contact contact);
    
    List<Contact> create(List<Contact> contacts);

    List<Contact> loadByName(String name);
    
    List<Contact> load();
    
    Optional<Contact> loadById(Long contactId);
}
