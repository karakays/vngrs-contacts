package com.vngrs.contacts.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vngrs.contacts.api.model.ContactDto;
import com.vngrs.contacts.api.model.CreateContactDto;
import com.vngrs.contacts.api.model.NameDto;
import com.vngrs.contacts.domain.Contact;
import com.vngrs.contacts.domain.PhoneNumber;
import com.vngrs.contacts.service.ContactService;
import com.vngrs.contacts.util.PhoneNumberUtils;

@RestController
public class ContactsController {
    
    private final ContactService contactService;
    
    public ContactsController(ContactService contactService) {
        this.contactService = contactService;
    }
    
    @GetMapping(value = "/contacts/{contactId}")
    public ResponseEntity<ContactDto> getContactById(@PathVariable Long contactId) {
        Optional<Contact> contact = contactService.loadById(contactId);
        
        return contact.isPresent() ? new ResponseEntity<ContactDto>(map(contact.get()), HttpStatus.OK) :
            new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    
    @GetMapping(value = "/contacts")
    public List<ContactDto> getContacts() {
        List<Contact> contacts = contactService.load();
        return map(contacts);
    }
    
    @GetMapping(value = "/contacts", params = "name")
    public List<ContactDto> getContactsByName(@RequestParam(required = true) String name) {
        List<Contact> contacts = contactService.loadByName(name);
        return map(contacts);
    }
    
    @PostMapping(value = "/contact")
    public ResponseEntity<Void> createContact(@Valid @RequestBody CreateContactDto contactDto) {
        NameDto name = contactDto.getName();
        PhoneNumber phone = PhoneNumberUtils.parse(contactDto.getPhone());
        Contact contact = new Contact(name.getFirstName(), name.getLastName(), Arrays.asList(phone));
        
        contact = contactService.create(contact);
        return ResponseEntity.noContent().build();
    }
    
    @PostMapping(value = "/contacts")
    public ResponseEntity<Void> createContacts(@Valid @RequestBody List<CreateContactDto> contactDto) {
        // filter duplicates and group by ContactDto.Name
        Map<NameDto, List<CreateContactDto>> contactMap = contactDto.stream()
                .distinct().collect(Collectors.groupingBy(c -> c.getName()));
        
        List<Contact> contacts = new ArrayList<>();
        
        for(Map.Entry<NameDto, List<CreateContactDto>> entry : contactMap.entrySet()) {
            NameDto name = entry.getKey();
            List<PhoneNumber> phones = entry.getValue().stream().map(c -> PhoneNumberUtils.parse(c.getPhone()))
                    .collect(Collectors.toList());
            contacts.add(new Contact(name.getFirstName(), name.getLastName(), phones));
        }
        
        contactService.create(contacts);
        return ResponseEntity.noContent().build();
    }
    
    private ContactDto map(Contact contact) {
        return new ContactDto(new NameDto(contact.getFirstName(), contact.getLastName()), 
                contact.getPhoneNumber().stream().map(c -> c.getPhoneNumber()).collect(Collectors.toList()));
    }
    
    private List<ContactDto> map(List<Contact> contacts) {
        return contacts.stream().map(c -> map(c)).collect(Collectors.toList());
    }
}
