package com.vngrs.contacts.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonUnwrapped;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class ContactDto {
    @JsonUnwrapped
    private final NameDto name;
    private final List<String> phones;
}
