package com.vngrs.contacts.api.model;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Represents name of the contact as a pair consisting of first and last name.
 * 
 * @author Selçuk Karakayalı
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class NameDto {
    @NotEmpty
    @JsonProperty("name")
    private String firstName;
    @NotEmpty
    private String lastName;
}
