package com.vngrs.contacts.api.model;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CreateContactDto {
    @NotEmpty
    private final String phone;
    @JsonUnwrapped
    private final NameDto name;
    
    @JsonCreator
    public CreateContactDto(@JsonProperty("phoneNumber") String phone, @JsonProperty("name") String firstName,
            @JsonProperty("lastName") String lastName) {
        this.phone = phone;
        this.name = new NameDto(firstName, lastName);
    }
}
