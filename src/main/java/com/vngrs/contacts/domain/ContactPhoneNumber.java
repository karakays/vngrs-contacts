package com.vngrs.contacts.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(of = {"phoneNumber"})
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"phone_number", "contact_id"})})
public class ContactPhoneNumber {
    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    
    @Column(name = "phone_number")
    private String phoneNumber;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contact_id", updatable = false, nullable = false)
    private Contact contact;

    public ContactPhoneNumber(String phoneNumber, Contact contact) {
        this.phoneNumber = phoneNumber;
        this.contact = contact;
    }
    
    public ContactPhoneNumber(PhoneNumber phoneNumber, Contact contact) {
        this(phoneNumber.getFullNumber(), contact);
    }
    
    public String toString() {
        return String.format("ContactPhoneNumber [id=%s, phoneNumber=%s, contactId=%s]",
                id, phoneNumber, contact.getId());
    }
}
