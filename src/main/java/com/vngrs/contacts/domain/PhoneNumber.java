package com.vngrs.contacts.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class PhoneNumber {
    private final String countryCode;
    private final String mobileNumber;
    
    public String getFullNumber() {
        return this.countryCode + this.mobileNumber;
    }
}
