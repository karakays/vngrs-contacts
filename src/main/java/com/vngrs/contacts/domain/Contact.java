package com.vngrs.contacts.domain;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Represents a contact, uniquely identified by this.firstName && this.lastName
 * 
 * * @author Selçuk Karakayalı
 */
@Data
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"firstName", "lastName"})})
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Contact {
    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;
   
    @Column
    private String firstName;
    
    @Column
    private String lastName;
    
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "contact", orphanRemoval = true)
    private Set<ContactPhoneNumber> phoneNumber;

    public Contact(String firstName, String lastName, List<PhoneNumber> phones) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phones.stream().map(p -> new ContactPhoneNumber(p.getFullNumber(), this))
                .collect(Collectors.toSet());
        this.createdDate = new Date();
    }
    
    public String toString() {
        return String.format("Contact [id=%s, firstName=%s, lastName=%s, phoneNumbers=%s]",
                id, firstName, lastName, phoneNumber);
    }
}
