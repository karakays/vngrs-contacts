

![](https://www.yildizteknopark.com.tr/kcfinder/upload/images/VNGRS-Red%282%29.png)


VNGRS challenge: contacts
============


### Cloning Repo
You can clone repository as below. Before that, just make sure I got your public ssh key added into access keys.

```
    $ git clone git@bitbucket.org:karakays/vngrs-contacts.git
    $ cd vngrs-contacts/
``` 
    
### Build
Get a build using maven.

```
	$ mvn clean package
```


### Boot
Don't forget to pass your environment variables as below. Boot application using java command. API_PORT is 8888 by default. You can override it as shown.

```
	$ export DB_USERNAME=root DB_PASSWORD=password DB_NAME=vngrs_db DB_URL=localhost:3306 API_PORT=8000
	$ java -jar target/contacts-0.0.1-SNAPSHOT.jar
```

If you you face any issues with environment variables, you can also use VM arguments instead, like

```
	$ unset DB_URL # remove variable
	$ java -jar -DDB_URL=localhost:3306 target/contacts-0.0.1-SNAPSHOT.jar	
```


### Notes

 - Requires JRE 1.8 or higher
 - Requires Maven
 - Tested with MySQL Server version: 5.7.21 Homebrew
